package com.company;



public class MyArray extends BaseArray {
   private int size = 0;

    public MyArray() {
        arr = new int[10];
    }

    @Override
    protected void add(int val) {
        arr[size] = val;
        size++;
        if (size == arr.length) {
            int[] newArray = new int[arr.length * 2];
            for (int i = 0; i < arr.length; i++) {
                newArray[i] = arr[i];
            }
            arr = newArray;
        }
    }

    @Override
    protected void remove(int index) {
        if (index >= 0 && index < size) {
            for (int i = index; i < (size - 1); i++) {
                arr[i] = arr[i + 1];
            }
            size--;
        }
    }

    void removeValue(String s) {
        int index = -1;
        for (int i = 0; i < size; i++) {
            if (s.equals(arr[i])) {
                index = i;
            }
        }
        if (index != -1) {
            remove(index);
        }
    }

    @Override
    protected void size() {

        System.out.println("Размер: " + size);
    }

    @Override
    protected void toStr() {
        for (int i = 0; i < size; i++) {
            System.out.println(" " + arr[i]);
        }
    }

    void getValueByIndex(int index) {

        System.out.println("Значение по елементу: "+ arr[index]);

    }
}
