package com.company;

abstract class BaseArray {
    protected int [] arr = null;

    protected abstract void add(int val);
    protected abstract void remove(int index);
    protected abstract void size();
    protected abstract void toStr();
}
